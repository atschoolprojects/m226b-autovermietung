﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRental
{
    public class Employee : Person
    {
        public string Job { get; set; }
        public bool IsWorking { get; set; }
        
        
        public Employee(string lastname, string firstname, DateTime birthday, string job) : base(lastname, firstname, birthday)
        {
           Job = job;
           IsWorking = false;
        }

        public void printInfos()
        {
            Console.WriteLine($"Lastname: {Lastname}\nFirstname: {Firstname}\nJob: {Job}\n") ;
        }

        public override void talk(string text, string speaker)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write($"\n<{speaker}>");
            

            base.talk(text, speaker);
        }

        public double calculateEndPrice(int numberOfRentedDays, double pricePerDay)
        {
            if (numberOfRentedDays <= 0 || pricePerDay <= 0)
            {
                Console.WriteLine("Somethin unexpected happened. Pleas restart the program.");

                return -1;
            }
            else
            {
                return numberOfRentedDays * pricePerDay;

            }
        }
    }
}
