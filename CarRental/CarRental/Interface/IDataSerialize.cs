﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarRental.Interface
{
    public interface IDataSerialize
    {
        object Deserialize<T>(string filePath);
    }
}
