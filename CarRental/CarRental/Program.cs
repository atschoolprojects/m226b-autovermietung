﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.OData.Edm;

namespace CarRental
{
    public class Program
    {

        static void Main(string[] args)
        {
            //initialise variables
            bool stayInStore = true;
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/CarRental";
           
            //Create storage and costumer object
            Storage storage = new Storage();
            Vehicle vehicle = new Vehicle("ZG 1","Bobycar",0,VehicleType.Car);
            Costumer costumer = new Costumer(1, "Gwerder", "Manuel", Date.Parse("2003-08-24"));


            //Creates the lists
            List<Vehicle> cars = new List<Vehicle>();
            List<Vehicle> trucks = new List<Vehicle>();
            List<Vehicle> motorcycles = new List<Vehicle>();
            List<Employee> employees = new List<Employee>();

            //Fill cars into the list
            if (File.Exists(filePath + "/cars.JSON"))
            {
                //Read the list of vehicles from the file
                cars = (List<Vehicle>)storage.Deserialize<Vehicle>(filePath + "/cars.JSON");
            }
            else
            {     
                cars.Add(new Vehicle("ZG 654 894", "Ford Nizza", 40.00, VehicleType.Car));
                cars.Add(new Vehicle("ZG 864 819", "Hyundai i30 N Project C", 45.50, VehicleType.Car));
                cars.Add(new Vehicle("ZG 134 654", "Opel Astra", 42.50, VehicleType.Car));

                storage.Serialize(employees, filePath + "/cars.JSON");

            }

            if (File.Exists(filePath + "/trucks.JSON"))
            {
                trucks = (List<Vehicle>)storage.Deserialize<Vehicle>(filePath + "/trucks.JSON");
            }
            else
            {               
                trucks.Add(new Vehicle("ZG 234 454", "Mercedes Actros", 85.00, VehicleType.Truck));
                trucks.Add(new Vehicle("ZG 344 240", "Mercedes Atego", 88.90, VehicleType.Truck));

                storage.Serialize(employees, filePath + "/trucks.JSON");

            }

            if (File.Exists(filePath + "/motorcycles.JSON"))
            {
                motorcycles = (List<Vehicle>)storage.Deserialize<Vehicle>(filePath + "/motorcycles.JSON");
            }
            else
            {
                motorcycles.Add(new Vehicle("ZG 823 45", "Honda GL1800 Gold Wing", 32.00, VehicleType.Motorcycle));
                motorcycles.Add(new Vehicle("ZG 74 234", "CRF1100L Africa Twin", 29.90, VehicleType.Motorcycle));

                storage.Serialize(employees, filePath + "/motorcycles.JSON");

            }


            if (File.Exists(filePath + "/employees.JSON"))
            {
                //Read the list of employees form the file
                employees = (List<Employee>)storage.Deserialize<Employee>(filePath + "/employees.JSON");
            }
            else
            {
                employees.Add(new Employee("", "", Date.Parse("2020-01-01"), "Reception"));
                employees.Add(new Employee("Huber", "Maya", Date.Parse("2000-01-16"), "Advisor"));
                employees.Add(new Employee("Weber", "Frank", Date.Parse("1970-12-06"), "Mechanic"));
                employees.Add(new Employee("Holtz", "Franziska", Date.Parse("1965-05-22"), "Return"));
                employees.Add(new Employee("Kursic", "Haris", Date.Parse("1999-05-31"), "Administration"));

                storage.Serialize(employees, filePath + "/employees.JSON");

            }


            costumer.enterStore(employees);

            costumer.wait(1000);           
            while (stayInStore)
            {
                employees[0].talk("These are our free employees.", employees[0].Job);
                int employeeCounter = 1;
                foreach (Employee employee in employees)
                {
                    //Shows all advisors which aren't working at the moment
                    if (!employee.IsWorking && employee.Job != "Reception")
                    {
                        Console.WriteLine(employeeCounter++);
                        employee.printInfos();
                    }
                }
                
                int employeeNr = getUserinputUntilItIsValid(0, "Choose an employee. (Employee Number)", employees[0].Job, employees.Count - 1, "employee");
                
                //employeeName get the value of the choosen emplyee's name
                string employeeName = employees[employeeNr].Firstname + " " + employees[employeeNr].Lastname;
                
                switch (employees[employeeNr].Job)
                {
                    case "Mechanic":

                        //The client talks with the Mechanic to repair the vehicle
                        employees[employeeNr].talk("Hello. Oh I see your vehicle is damaged. Don't worry I can repair that. There are no costs for you.", employeeName);
                        break;


                    case "Return":

                        //The client talks with the Returnservice to return his rented vehicle
                        employees[employeeNr].talk("Hello. Thank you for returning your vehicle.", employeeName);

                        //Print all Vehicle Types
                        vehicle.printAllTypes();

                        //Get the number of the vehicle type
                        int rentedVehicleTypeNr = getUserinputUntilItIsValid(employeeNr, "Which type of vehicle have you rented? (Number)", employeeName, Enum.GetNames(typeof(VehicleType)).Length, "vehicle type");

                        VehicleType choosenReturnVehicleType = (VehicleType)rentedVehicleTypeNr;

                        employees[employeeNr].talk($"Ok, a {choosenReturnVehicleType}. Here are all our {choosenReturnVehicleType}s.", employeeName);

                        int choosenReturnVehicleNr = 0;

                        switch (choosenReturnVehicleType)
                        {
                            case VehicleType.Car:

                                int carCounter = 0;
                                foreach (Vehicle car in cars)
                                {
                                    car.printInfos(carCounter++);
                                }

                                //get the number of the car
                                choosenReturnVehicleNr = getUserinputUntilItIsValid(employeeNr, "Which car do you want to return? (Number)", employeeName, carCounter, "car");
                                break;


                            case VehicleType.Truck:

                                int truckCounter = 0;
                                foreach (Vehicle truck in trucks)
                                {
                                    truck.printInfos(truckCounter++);
                                }

                                //get the number of the truck
                                choosenReturnVehicleNr = getUserinputUntilItIsValid(employeeNr, "Which truck do you want to return? (Number)", employeeName, truckCounter, "car");
                                break;

                            case VehicleType.Motorcycle:

                                int motorcycleCounter = 0;
                                foreach (Vehicle motorcycle in motorcycles)
                                {
                                    motorcycle.printInfos(motorcycleCounter++);
                                }

                                //get the number of the motorcycle
                                choosenReturnVehicleNr = getUserinputUntilItIsValid(employeeNr, "Which motorcycle do you want to return? (Number)", employeeName, motorcycleCounter, "car");
                                break;

                            default:
                                break;

                        }

                        //If the choosenNr of the choosen Type is not rented
                        if (!cars[choosenReturnVehicleNr].IsRented && choosenReturnVehicleType == VehicleType.Car || !trucks[choosenReturnVehicleNr].IsRented && choosenReturnVehicleType == VehicleType.Truck || !motorcycles[choosenReturnVehicleNr].IsRented && choosenReturnVehicleType == VehicleType.Motorcycle)
                        {
                            employees[employeeNr].talk("You can't return this vehicle.", employeeName);
                        }
                        else
                        {
                            
                            employees[employeeNr].talk($"Ok let's to the check.\n1: nothing\n2: wash the car\n3: clean inner space\n4: both", employeeName);

                            int choosenReturnService = getUserinputUntilItIsValid(employeeNr, "What do we have to do?", employeeName, 4, "returnservice");

                            employees[employeeNr].talk("Ok, thank you for your help. I will do that.", employeeName);

                            switch (choosenReturnVehicleType)
                            {
                                case VehicleType.Car:

                                    //Return the car
                                    cars[choosenReturnVehicleNr].getReturned();

                                    //save the car in the file
                                    storage.Serialize(cars, filePath + "/cars.JSON");
                                    break;

                                case VehicleType.Truck:

                                    //Return the truck
                                    trucks[choosenReturnVehicleNr].getReturned();

                                    //save the truck in the file
                                    storage.Serialize(trucks, filePath + "/trucks.JSON");
                                    break;

                                case VehicleType.Motorcycle:

                                    //Return the motorcycle
                                    motorcycles[choosenReturnVehicleNr].getReturned();

                                    //save the motorcycle in the file
                                    storage.Serialize(motorcycles, filePath + "/motorcycles.JSON");
                                    break;

                                default:
                                    writeGeneralErrorMessage();
                                    break;
                            }

                        }

                        break;


                    case "Administration":

                        //the client talks with the Administrator to see something in the system
                        employees[employeeNr].talk("Hello. What can I do for you?", employeeName);
                        Console.WriteLine("1: Show your latest reservations\n2: Show the statistics");

                        //get the number of the service
                        int administrationService = getUserinputUntilItIsValid(employeeNr, "What can I do for you?",employeeName,2,"administrationservice");

                        switch (administrationService)
                        {
                            case 1:
                                //The client wants to see his latest reservations
                                employees[employeeNr].talk("I'm sorry, but this store doesn't excist long enough for reservationhistory", employeeName);
                                break;

                            case 2:
                                //The client wants to see the statisitcs of the store
                                employees[employeeNr].talk("I'm sorry, but this store doesn't excist long enough for statistics", employeeName);
                                break;

                            default:
                                writeGeneralErrorMessage();
                                break;
                        }
                        break;


                    case "Advisor":

                        //The client talks with an advisor to rent a vehicle
                        employees[employeeNr].talk("Hello. These are owr vehicle types which you can rent.", employeeName);

                        //Print all Vehicle Types
                        vehicle.printAllTypes();
                        
                        //Get the number of the vehicle type
                        int vehicleTypeNr = getUserinputUntilItIsValid(employeeNr, "Which type of vehicle do you want to rent? (Number)",employeeName, Enum.GetNames(typeof(VehicleType)).Length,"vehicle type");

                        VehicleType choosenVehicleType = (VehicleType)vehicleTypeNr;

                        employees[employeeNr].talk($"Ok, a {choosenVehicleType}. Here are all our vehicles of this type:", employeeName);

                        int choosenVehicleNr = 0;
                        double price = 0;
                        string vehicleName = "";
                        switch (choosenVehicleType)
                        {
                            case VehicleType.Car:
                                int carCounter = 0;
                                foreach (Vehicle car in cars)
                                {                                    
                                        car.printInfos(carCounter++);                                    
                                }

                                //get the number of the car
                                choosenVehicleNr = getUserinputUntilItIsValid(employeeNr, "Which car do you want to rent? (Number)", employeeName, carCounter, "car");

                                //set the price
                                price = cars[choosenVehicleNr].PricePerDay;

                                //set the name
                                vehicleName = cars[choosenVehicleNr].Name;
                                break;

                            case VehicleType.Truck:
                                int truckCounter = 0;
                                foreach (Vehicle truck in trucks)
                                {                                   
                                        truck.printInfos(truckCounter++);                                    
                                }

                                //get the number of the truck
                                choosenVehicleNr = getUserinputUntilItIsValid(employeeNr, "Which truck do you want to rent? (Number)", employeeName, truckCounter, "truck");

                                //set the price
                                price = trucks[choosenVehicleNr].PricePerDay;

                                //set the name
                                vehicleName = trucks[choosenVehicleNr].Name;

                                break;

                            case VehicleType.Motorcycle:
                                int motorcycleCounter = 0;
                                foreach (Vehicle motorcycle in motorcycles)
                                {
                                        motorcycle.printInfos(motorcycleCounter++);                                    
                                }

                                //get the number of the motorcycle
                                choosenVehicleNr = getUserinputUntilItIsValid(employeeNr, "Which motorcycle do you want to rent? (Number)", employeeName, motorcycleCounter, "motorcycle");

                                //set the price
                                price = motorcycles[choosenVehicleNr].PricePerDay;

                                //set the name
                                vehicleName = motorcycles[choosenVehicleNr].Name;
                                break;
                            default:
                                writeGeneralErrorMessage();
                                break;
                        }

                        //If the choosenNr of the choosen Type is rented
                        if (cars[choosenVehicleNr].IsRented && choosenVehicleType == VehicleType.Car || trucks[choosenVehicleNr].IsRented && choosenVehicleType == VehicleType.Truck || motorcycles[choosenVehicleNr].IsRented && choosenVehicleType == VehicleType.Motorcycle)
                        {                            
                            employees[employeeNr].talk("You can't rent this vehicle.", employeeName);
                        }
                        else
                        {
                            //get the number of days the client wants to rent the vehicle
                            int numberOfRentedDays = getUserinputUntilItIsValid(employeeNr, "How many days would you like to rent it? (min. 1 day, max. 1 year)", employeeName, 356, "number of days", 1);

                            //calculate the endprice
                            double endPrice = employees[employeeNr].calculateEndPrice(numberOfRentedDays, price);
                            employees[employeeNr].talk($"So, that would cost {endPrice} CHF", employeeName);

                            //ask for the payment method and show the options
                            employees[employeeNr].talk("How do you want to pay?\n1: Cash\n2: Credit card", employeeName);

                            //get the number of the payment method
                            int paymentMethodNr = getUserinputUntilItIsValid(employeeNr, "How do you want to pay?", employeeName, 2, "payment method");

                            double paid = 0;
                            double change = 0;
                            switch (paymentMethodNr)
                            {
                                case 1:
                                    //get the amout of money the user has paid.
                                    paid = getUserinputUntilItIsValid(employeeNr, "How much worth does your cash have?", employeeName, int.MaxValue, "amount of money", endPrice);
                                    if (paid > endPrice)
                                    {
                                        //client has paid too much-- > give the change
                                        change = paid - endPrice;
                                        employees[employeeNr].talk($"Thank you and {change} CHF back", employeeName);
                                    }
                                    break;

                                case 2:
                                    //pay with Credit card
                                    employees[employeeNr].talk("Pleas type in your Pin here...", employeeName);
                                    costumer.wait(500);

                                    paid = endPrice;

                                    employees[employeeNr].talk("Thank you", employeeName);
                                    break;


                                default:
                                    writeGeneralErrorMessage();
                                    break;
                            }

                            //Print the receipt
                            employees[employeeNr].talk($"Here is your receipt:\n----------\n{vehicleName}\n\nPrice per day: {price} CHF\nRented days: {numberOfRentedDays}\nTotal: {endPrice} CHF\nPaid: {paid} CHF\nChange: {change} CHF\nThank you for your visit\n----------", employeeName);

                            //Hand out the keys
                            employees[employeeNr].talk("Here are your keys.", employeeName);

                            switch (choosenVehicleType)
                            {
                                case VehicleType.Car:

                                    //Rent the car
                                    cars[choosenVehicleNr].getRented();

                                    //save the car in the file
                                    storage.Serialize(cars, filePath + "/cars.JSON");
                                    break;

                                case VehicleType.Truck:

                                    //Rent the truck
                                    trucks[choosenVehicleNr].getRented();

                                    //save the truck in the file
                                    storage.Serialize(trucks, filePath + "/trucks.JSON");
                                    break;

                                case VehicleType.Motorcycle:

                                    //Rent the motorcycle
                                    motorcycles[choosenVehicleNr].getRented();

                                    //save the motorcycle in the file
                                    storage.Serialize(motorcycles, filePath + "/motorcycles.JSON");
                                    break;
                                default:
                                    writeGeneralErrorMessage();
                                    break;
                            }
                        }
                                                                     
                        //Say Goodbye
                        employees[employeeNr].talk("Goodbye and have a nice day.", employeeName);
                                                
                        break;

                    default:
                        writeGeneralErrorMessage();
                        break;
                }

                //Mabey the client want to do an other thing in the store
                employees[0].talk("Do you want to stay in the store? (Y/N)", employees[0].Job);

                string answer = Console.ReadLine();

                if (answer == "Y" || answer == "y")
                {
                    Console.WriteLine("Ok");
                }
                else if (answer == "N" || answer == "n")
                {
                    costumer.leaveStore(employees);
                    stayInStore = false;
                }
                else
                {
                    Console.WriteLine("You canceled the conversation and stay in the store.");
                }

            }

            int getUserinputUntilItIsValid(int speakerNr, string text, string speakerName, int maximum, string type, double minimum = 0)
            {
                bool isUserinputValid = false;
                int validUserinput = -1;

                while (!isUserinputValid)
                {
                    //while the userinput isn't valid the user will be asked to choose an option
                    employees[speakerNr].talk(text, speakerName);
                    (isUserinputValid, validUserinput) = validateUserInput(isUserinputValid, maximum, type, minimum);
                }
                return validUserinput;
            }


            (bool, int) validateUserInput(bool isValid, int maximum, string type, double minimum = 0)
            {
                int userInputNr = -1;
              

                //checks if the input was valid
                try
                {
                    //Read the user input
                    userInputNr = Int32.Parse(Console.ReadLine());

                    if (userInputNr >= minimum && userInputNr <= maximum)
                    {
                        //user input was valid
                        isValid = true;
                    }
                    else
                    {
                        //user input was invalid (number to high or low)
                        employees[0].talk($"This {type} isn't available.", employees[0].Job);
                    }
                }
                catch (FormatException)
                {
                    // user input was invalid (no number)
                    employees[0].talk("Please type in a number!", employees[0].Job);
                }

                return (isValid, userInputNr);
            }

            void writeGeneralErrorMessage()
            {
                //Exception
                employees[0].talk("We're verry sorry, but something unexpected happened.", employees[0].Job);
            }
        }
    }
}
