﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CarRental;
using CarRental.Interface;
using System;

namespace CarRental_Test
{
    [TestClass]
    public class Serialize_Test
    {
        [TestMethod]
        public void Deserialize_ReturnObject()
        {
            stubStorage stubStorage = new stubStorage();
            var time = stubStorage.Deserialize<DateTime>("testPath");
            Assert.IsNotNull(time);
        }
    }

    class stubStorage : IDataSerialize
    {
        public object Deserialize<T>(string filePath)
        {
            DateTime dateTime = new DateTime(2021,1,1);
            return dateTime;
        }
    }
}
