﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Exercises
{
    class writeInTxt
    {
        public void writeIntoTxt()
        {
            string fileName = @"C:\Users\Manuel\Documents\m226b-autovermietung\Files\textValues.txt";

            //create StreamWriter-object
            StreamWriter sw = new StreamWriter(fileName);

            sw.WriteLine(1234);
            sw.WriteLine(5.678);
            sw.WriteLine("Hello friends; Special symbols: ä ö ü ");
            sw.Close();

            //create StreamReader-object
            StreamReader sr = new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read), Encoding.UTF8);

            //read
            Console.WriteLine("Content of File: \"{0}\":", ((FileStream)sr.BaseStream).Name);

            //write on console
            for (int count = 0; sr.Peek() >= 0; count++)
            {
                Console.WriteLine("{0}:\t{1}", count, sr.ReadLine());
            }

            //close
            sr.Close();
        }
    }
}
